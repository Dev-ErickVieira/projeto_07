$(function(){

	$('.menu-mobile-content a').click(function(){
		$('.menu-mobile-content').css('width','0');
		$('.conteudo-header').css('position','relative').css('z-index','2');
		$('.sobre h2').css('position','relative').css('z-index','2');
		$('.sobre p').css('position','relative').css('z-index','2');
	});

	$('header ul li a').click(function(){
		var href = $(this).attr('href');

		var offSetTop = $(href).offset().top;

		$('html,body').animate({'scrollTop':offSetTop},1000);

		return false;
	});

	$('.menu-mobile').click(function(){
		$('.menu-mobile-content').css('width','100%');
		$('.conteudo-header').css('position','static').css('z-index','0');
		$('.sobre h2').css('position','static').css('z-index','0');
		$('.sobre p').css('position','static').css('z-index','0');
	});

	$('.closeBtn').click(function(){
		$('.menu-mobile-content').css('width','0');
		$('.conteudo-header').css('position','relative').css('z-index','2');
		$('.sobre h2').css('position','relative').css('z-index','2');
		$('.sobre p').css('position','relative').css('z-index','2');
	});




});